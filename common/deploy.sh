#!/bin/sh

if [ ! -d docker_deploy ]; then
	git clone http://172.17.23.195:10080/QNAP/docker_deploy.git docker_deploy || exit 1
	sed -i 's/`date +%s`/$2/g' docker_deploy/container_setup.sh || exit 1
	sed -i 's/-ti/-tid/g' docker_deploy/container_setup.sh || exit 1
	sed -i 's/-v \/mnt\/pub:\/mnt\/pub:ro/-v \/mnt\/pub:\/mnt\/pub:ro -v \/mnt\/data\/$USER:\/root\/extension/g' docker_deploy/container_setup.sh || exit 1
	sed -i 's/--rm//g' docker_deploy/container_setup.sh || exit 1
fi
